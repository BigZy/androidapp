REM Specify the path to the installed JDK
REM Укажите путь к установленной JDK
SET JAVA_HOME=C:\Program Files\Java\jdk1.8.0_121\bin

REM Allocate memory for the Auth Server
REM Minimum: 64Mb
REM Выделяем оперативную память под сервер авторизации
REM Минимально: 64Mb
SET HEAP_SIZE=-Xmx256m
