ECHO OFF
REM  QBFC Project Options Begin
REM  HasVersionInfo: Yes
REM  Companyname: 
REM  Productname: Lineage 2 High Five Part 5
REM  Filedescription: Auth Server Console
REM  Copyrights: GRIND-TEAM
REM  Trademarks: 
REM  Originalname: 
REM  Comments: 
REM  Productversion: 00.00.00.01
REM  Fileversion: 00.00.00.01
REM  Internalname: 
REM  Appicon: ..\..\..\..\..\..\..\grind-team\web\designe\api_2305.ico
REM  QBFC Project Options End
ECHO ON
@echo off
cls

title server
color 0B

REM Include config
call StartAuthServer.cmd

:start
echo Initialize Auth Server ...
echo.

REM Run java machine
"%JAVA_HOME%\java" -server -Dfile.encoding=UTF-8 %HEAP_SIZE% -cp config/xml;../serverlibs/*; core.Server

if ERRORLEVEL 2 goto restart
if ERRORLEVEL 1 goto error
goto end

:restart
echo.
echo Auth Server restarted!
echo.
goto start

:error
echo.
echo Auth Server terminated abnormaly!
echo.

:end
echo.
echo Auth Server terminated!
echo.

pause

