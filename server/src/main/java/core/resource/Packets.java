package core.resource;

import core.model.Packet;
import core.network.GMapMarkers;

import java.util.HashMap;

/**
 * Created by BigZy on 04.03.2018.
 */
public class Packets {
    private static HashMap<Integer, Packet> packets = new HashMap();

    public static void initPackets() {
        packets.put(1, new GMapMarkers());
    }

    public static HashMap<Integer, Packet> getPackets() {
        return packets;
    }
}
