package core;

import org.jboss.netty.bootstrap.ServerBootstrap;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;
import core.model.ServerPipelineFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static core.resource.Packets.initPackets;

/**
 * Created by BigZy on 04.03.2018.
 */
public class Server {
    public static void main(String[] args) {
        //подгрузка ресурсов
        initPackets();
        //инициализация сервера
        ExecutorService bossExec = Executors.newFixedThreadPool(1);
        ExecutorService ioExec = Executors.newFixedThreadPool(4);
        ServerBootstrap server = new ServerBootstrap(new NioServerSocketChannelFactory(bossExec, ioExec, 4));
        server.setOption("connectTimeoutMillis", Integer.valueOf(10000));
        server.setPipelineFactory(new ServerPipelineFactory());
        server.bind(new InetSocketAddress("192.168.0.104", 25565));
        System.out.println("Server started");
    }
}
