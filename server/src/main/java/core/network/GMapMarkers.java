package core.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.jboss.netty.buffer.ChannelBuffer;
import core.model.Marker;
import core.model.Packet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by BigZy on 04.03.2018.
 */
public class GMapMarkers extends Packet {
    @Override
    public void get(ChannelBuffer buffer) {
        //тело получения пустое т.к. клиент не поылает этот пакет
    }

    @Override
    public void send(ChannelBuffer buffer) {
        //тестовые данные
        List<Marker> markers = new ArrayList<>();
        markers.add(new Marker(59.929329, 30.356764));
        markers.add(new Marker(59.935874, 30.317429));
        markers.add(new Marker(59.963372, 30.307403));
        //инициализация gson
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        //переводим аррей лист в json формат
        String markersToJson = gson.toJson(markers);

        //посылаем данные клиенту
        buffer.writeShort(markersToJson.length());

        for(int i = 0; i < markersToJson.length(); ++i) {
            buffer.writeChar(markersToJson.charAt(i));
        }

        System.out.println("Отправил данные");
    }

    @Override
    public int getId() {
        return 1;
    }
}
