package core.model;

/**
 * Created by BigZy on 04.03.2018.
 */
public class Marker {
    private double x;
    private double y;

    public Marker(double x, double y) {
        this.x = x;
        this.y = y;
    }
}
