package core.model;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import core.handlers.PacketDecoder;
import core.handlers.PacketEncoder;
import core.handlers.UserHandler;

/**
 * Created by BigZy on 04.03.2018.
 */
public class ServerPipelineFactory implements ChannelPipelineFactory {
    @Override
    public ChannelPipeline getPipeline() throws Exception {
        PacketDecoder decoder = new PacketDecoder();
        PacketEncoder encoder = new PacketEncoder();
        return Channels.pipeline(decoder, encoder, new UserHandler());
    }
}
