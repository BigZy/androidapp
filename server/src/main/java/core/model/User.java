package core.model;

import org.jboss.netty.channel.Channel;
import core.network.GMapMarkers;
import core.resource.Packets;

/**
 * Created by BigZy on 04.03.2018.
 */
public class User {
    private String email;
    private boolean authorized;

    private Channel channel;

    public User(Channel channel) {
        this.channel = channel;
        System.out.println("User connected and created");
        System.out.println("Отправка пакета");
        sendPacket(Packets.getPackets().get(1));
        System.out.println("Отправил");
    }

    //отправка пакета
    public void sendPacket(Packet packet) {
        channel.write(packet);
    }

    //действия с пакетами
    public void acceptPacket(Packet packet) {
        if (packet instanceof GMapMarkers) {
            GMapMarkers gMapMarkers = (GMapMarkers) packet;

        }// else if ...
    }
}
