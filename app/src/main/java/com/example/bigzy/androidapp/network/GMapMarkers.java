package com.example.bigzy.androidapp.network;

import com.example.bigzy.androidapp.model.Marker;
import com.example.bigzy.androidapp.model.Packet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.jboss.netty.buffer.ChannelBuffer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by BigZy on 04.03.2018.
 */

public class GMapMarkers extends Packet {
    private List<Marker> markerList;

    @Override
    public void get(ChannelBuffer buffer) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();

        int length = buffer.readShort();
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < length; ++i)
            builder.append(buffer.readChar());

        Type listType = new TypeToken<List<Marker>>() {}.getType();
        markerList = gson.fromJson(builder.toString(), listType);
        for (Marker m : markerList)
            System.out.println(m.getX() + " " + m.getY());
    }

    @Override
    public void send(ChannelBuffer buffer) {}

    @Override
    public int getId() {
        return 1;
    }

    public List<Marker> getMarkerList() {
        return markerList;
    }
}
