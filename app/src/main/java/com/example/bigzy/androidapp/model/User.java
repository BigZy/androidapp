package com.example.bigzy.androidapp.model;

import com.example.bigzy.androidapp.network.GMapMarkers;
import com.example.bigzy.androidapp.resource.Packets;

import org.jboss.netty.channel.Channel;

import java.util.List;

/**
 * Created by BigZy on 04.03.2018.
 */

public class User {
    private String email;
    private boolean authorized;

    private static List<Marker> markerList;

    private Channel channel;

    public User(Channel channel) {
        this.channel = channel;
        sendPacket(Packets.getPackets().get(1));
    }

    //отправка пакета
    public void sendPacket(Packet packet) {
        channel.write(packet);
    }

    //действия с пакетами
    public void acceptPacket(Packet packet) {
        if (packet instanceof GMapMarkers) {
            GMapMarkers gMapMarkers = (GMapMarkers) packet;

            markerList = gMapMarkers.getMarkerList();
        }// else if ...
    }

    public static List<Marker> getMarkerList() {
        return markerList;
    }
}
