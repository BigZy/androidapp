package com.example.bigzy.androidapp.model;

import com.example.bigzy.androidapp.handlers.PacketDecoder;
import com.example.bigzy.androidapp.handlers.PacketEncoder;
import com.example.bigzy.androidapp.handlers.UserHandler;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;

/**
 * Created by BigZy on 04.03.2018.
 */

public class ClientPipelineFactory implements ChannelPipelineFactory {
    @Override
    public ChannelPipeline getPipeline() throws Exception {
        PacketDecoder decoder = new PacketDecoder();
        PacketEncoder encoder = new PacketEncoder();
        return Channels.pipeline(decoder, encoder, new UserHandler());
    }
}
