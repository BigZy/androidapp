package com.example.bigzy.androidapp.model;

/**
 * Created by BigZy on 04.03.2018.
 */

public class Marker {
    private double x;
    private double y;

    public Marker(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }
}
