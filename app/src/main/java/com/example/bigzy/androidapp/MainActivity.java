package com.example.bigzy.androidapp;

import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.bigzy.androidapp.fragments.ImportFragment;
import com.example.bigzy.androidapp.model.ClientPipelineFactory;
import com.example.bigzy.androidapp.model.User;
import com.example.bigzy.androidapp.resource.Packets;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import java.net.InetSocketAddress;
import java.util.concurrent.Executors;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    SupportMapFragment sMapFragment;

    private static final LatLng FIRST = new LatLng(59.929329, 30.356764);
    private static final LatLng SECOND = new LatLng(59.935874, 30.317429);
    private static final LatLng THIRD = new LatLng(59.963372, 30.307403);

    private Marker mPerth;
    private Marker mSydney;
    private Marker mBrisbane;

    private GoogleMap map;

    private Channel channel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        /*//инициализация инстансов пакетов
        Packets.initPackets();
        //инициализация подключения сервера
        ClientBootstrap clientBootstrap = new ClientBootstrap(new NioClientSocketChannelFactory(
                Executors.newCachedThreadPool(), Executors.newCachedThreadPool()));
        clientBootstrap.setPipelineFactory(new ClientPipelineFactory());
        System.out.println("1");
        ChannelFuture future = clientBootstrap.connect(new InetSocketAddress("192.168.0.103", 9999));
        System.out.println("2");
        channel = future.getChannel();*/

        super.onCreate(savedInstanceState);


        sMapFragment = SupportMapFragment.newInstance();

        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        FragmentManager fm = getFragmentManager();
        android.support.v4.app.FragmentManager sFm = getSupportFragmentManager();
        if (!sMapFragment.isAdded())
            sFm.beginTransaction().add(R.id.map, sMapFragment).commit();
        else
            sFm.beginTransaction().show(sMapFragment).commit();
        //fm.beginTransaction().replace(R.id.content_frame, new MainFragment()).commit();

        sMapFragment.getMapAsync(this);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        FragmentManager fm = getFragmentManager();
        android.support.v4.app.FragmentManager sFm = getSupportFragmentManager();

        int id = item.getItemId();

        if (sMapFragment.isAdded())
            sFm.beginTransaction().hide(sMapFragment).commit();

        if (id == R.id.nav_camera) {
            fm.beginTransaction().replace(R.id.content_frame, new ImportFragment()).commit();
        } else if (id == R.id.nav_gallery) {
            if (!sMapFragment.isAdded())
                sFm.beginTransaction().add(R.id.map, sMapFragment).commit();
            else
                sFm.beginTransaction().show(sMapFragment).commit();
        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {
            Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:89874041777"));
            startActivity(intent);
        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        User.getMarkerList();
        for (com.example.bigzy.androidapp.model.Marker marker : User.getMarkerList()) {
            System.out.println(marker.getX() + " " + marker.getY());
        }
        for (com.example.bigzy.androidapp.model.Marker marker : User.getMarkerList()) {
            map.addMarker(new MarkerOptions()
            .position(new LatLng(marker.getX(), marker.getY()))
            .title("Ретсоран")
            .snippet("Бар"));
        }
        LatLng sydney = new LatLng(-33.867, 151.206);

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(sydney, 13));

        googleMap.addMarker(new MarkerOptions()
                .title("Sydney")
                .snippet("The most populous city in Australia.")
                .position(sydney)
        );

        //map.moveCamera(CameraUpdateFactory.newLatLngZoom(FIRST, 10));
        // Add some markers to the map, and add a data object to each marker.
        mPerth = map.addMarker(new MarkerOptions()
                .position(FIRST)
                .title("Ресторан")
                .snippet("Бар"));
        System.out.println(mPerth.getId());

        mSydney = map.addMarker(new MarkerOptions()
                .position(SECOND)
                .title("Ресторан")
                .snippet("Italy"));

        mBrisbane = map.addMarker(new MarkerOptions()
                .position(THIRD)
                .title("Ресторан")
                .snippet("Capuletti"));
        mBrisbane.setTag(0);

        // Set a listener for marker click.
        map.setOnMarkerClickListener(this);
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        /*Toast toast = Toast.makeText(this, "Hello Android 7",Toast.LENGTH_LONG);
        toast.show();*/

        System.out.println(marker.getId());

        //TODO переделать по уму
        switch (marker.getId()) {
            case "m0" : {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(FIRST, 15));
                Toast.makeText(this, "m0", Toast.LENGTH_SHORT).show();
                break;
            }
            case "m1" : {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(SECOND, 15));
                Toast.makeText(this, "m1", Toast.LENGTH_SHORT).show();
                break;
            }
            case "m2" : {
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(THIRD, 15));
                Toast.makeText(this, "m2", Toast.LENGTH_SHORT).show();
                break;
            }
        }

        Snackbar.make(this.getCurrentFocus(), marker.getSnippet(), 30000)
                .setAction("Action", null).show();

        return false;
    }


}