package com.example.bigzy.androidapp.resource;

import com.example.bigzy.androidapp.model.Packet;
import com.example.bigzy.androidapp.network.GMapMarkers;

import java.util.HashMap;

/**
 * Created by BigZy on 04.03.2018.
 */

public class Packets {
    private static HashMap<Integer, Packet> packets = new HashMap();

    public static void initPackets() {
        packets.put(1, new GMapMarkers());
    }

    public static HashMap<Integer, Packet> getPackets() {
        return packets;
    }
}
